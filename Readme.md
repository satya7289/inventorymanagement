[![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](#)
[![Generic badge](https://img.shields.io/badge/Django-2.1.7-<COLOR>.svg)](https://www.djangoproject.com)
[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![PyPI license](https://img.shields.io/pypi/l/ansicolortags.svg)](https://pypi.python.org/pypi/ansicolortags/)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)

# **Sport's Inventory Management**
## **Table of content**
1. Abstraction
2. Prerequisites
3. Installation
4. Author
5. Contributors
<br>

## **Abstraction**
- Sport's Management Portal.
- Checkout, Checkout Functionality

## **Prerequisited**
- Django 2.1.7
- Python
- Bootstrap
- Ajax
- RestAPI
- JavaScript/HTML/CSS


## **Installation**

## **Author**
- **Satya Prakash**

## **Contributors**
- Satya
